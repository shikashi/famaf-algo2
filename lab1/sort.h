#ifndef _SORT_H
#define _SORT_H


/*static inline*/ void swap(int *a, int length, int i, int j);
/*
    Swap the value at position 'i' with the value at position 'j' in the
    array 'a'. Both 'i' and 'j' must be valid positions in the array.
	NOTE: Calling this function will increase the swaps counter used to
	measure the performance of the sorting algorithms. So don't call it
	unless as part of the implementation of one such algorithms!

*/

/*static inline*/ bool compare(int a, int b, bool ascending);
/*
	Compare 'a' with 'b' according to 'ascending'.
	If 'ascending' then returns a < b, else returns b < a.
	NOTE: Calling this function will increase the comparison counter used to
	measure the performance of the sorting algorithms. So don't call it
	unless as part of the implementation of one such algorithms!
*/

int min_max_pos_from(int *a, int length, int i, bool ascending);
/*
    Return the position of the minimum (or maximum, if ascending is false) value in 
    the array 'a' starting at position 'i'. The array 'a' must have exactly 'length' 
    elements, and 'i' must be a valid position in the array.

*/

void selection_sort(int *a, int length, bool ascending);
/*
    Sort the array 'a' using the Selection sort algorithm. The sort order
    depends on the 'ascending' argument.

    The array 'a' must have exactly 'length' elements.
    
    Number of comparisons: n(n-1)/2
    Number of swaps: n-1

*/

void insertion_sort(int *a, int length, bool ascending);
/*
    Sort the array 'a' using the Insertion sort algorithm. The sort order
    depends on the 'ascending' argument.

    The array 'a' must have exactly 'length' elements.

*/

void bubble_sort(int *a, int length, bool ascending);
/*
    Sort the array 'a' using the Bubble sort algorithm. The sort order
    depends on the 'ascending' argument.

    The array 'a' must have exactly 'length' elements.

*/

void quick_sort(int *a, int length, bool ascending, bool random_pivot);
/*
	Sort the array 'a' using the Quick Sort algorithm. The sort order depends on the 'ascending' argument.
	The array 'a' must have exactly 'length' elements.
*/

void quick_sort_recursive(int *a, int length, int izq, int der, bool ascending, bool random_pivot);

int pivot(int* a, int length, int izq, int der, bool ascending, bool random_pivot);

int get_swaps(void);
/*
	Returns the number of swaps when sorting the array.
*/

int get_comps(void);
/*
	Returns the number of comparisons when sorting the array.
*/


#endif // _SORT_H
