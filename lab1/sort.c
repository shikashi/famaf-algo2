#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "array_helpers.h"
#include "sort.h"


static int Qswap;
static int Qcomp;

int get_swaps(void) {
	return Qswap;
}

int get_comps(void) {
	return Qcomp;
}


static bool is_valid_position(int length, int i) {
    return(0 <= i && i < length);
}

/*inline static*/ void swap(int *a, int length, int i, int j) {
    assert(a != NULL);
    assert(length >= 0);
    assert(is_valid_position(length, i));
    assert(is_valid_position(length, j));

    Qswap++;

	int tmp = a[i];
	a[i] = a[j];
	a[j] = tmp;
}

/**
 * If (ascending) return a "less than" b
 * else return a "greater than" b
 */
bool compare(int a, int b, bool ascending){
	Qcomp++;
	
	if (ascending)
		return a < b;
	else
		return b < a;
}

int min_max_pos_from(int *a, int length, int i, bool ascending) {
    int result = i;

    /* Check preconditions */
    assert(a != NULL);
    assert(length >= 0);
    assert(is_valid_position(length, i));

    for(int j = i + 1; j < length; j++)
    	if (compare(a[j], a[result], ascending))
    		result = j;

    /* Check postconditions */
    assert(is_valid_position(length, result));
    return(result);
}

void selection_sort(int *a, int length, bool ascending) {
    /* Check preconditions */
    assert(a != NULL);
    assert(length >= 0);

	for(int j, i = 0; i < length - 1; i++) {
		j = min_max_pos_from(a, length, i, ascending);
		swap(a, length, i, j);
	}

    /* Check postconditions */
    assert(array_is_sorted(a, length, ascending));
}

void insertion_sort(int *a, int length, bool ascending) {
    /* Check preconditions */
    assert(a != NULL);
    assert(length >= 0);

	for(int i = 1; i < length; i++) {
		int j = i;
		while (j > 0 && compare(a[j], a[j-1], ascending)) {
			swap(a, length, j, j-1);
			j--;
		}
	}

    /* Check postconditions */
    assert(array_is_sorted(a, length, ascending));
}

void bubble_sort(int *a,int length, bool ascending) {
     /* Check preconditions */
    assert(a != NULL);
    assert(length >= 0);

	bool swapped = true;

	for(int i = length - 1; swapped && i > 0; i--) {
		swapped = false;
		for(int j = 0; j < i; j++) {
			if(compare(a[j], a[j+1], !ascending)) {
				swap(a, length, j, j+1);
				swapped = true;
			}
		}
	}

    /* Check postconditions */
    assert(array_is_sorted(a, length, ascending));
}

void quick_sort(int *a, int length, bool ascending, bool random_pivot) {
	/* Check preconditions */
    assert(a != NULL);
    assert(length >= 0);
    
    if (random_pivot)
    	srandom(time(NULL)); // you should be reading /dev/[u]random

	quick_sort_recursive(a, length, 0, length - 1, ascending, random_pivot);

    /* Check postconditions */
    assert(array_is_sorted(a, length, ascending));
}

void quick_sort_recursive(int *a, int length, int izq, int der, bool ascending, bool random_pivot) {

	assert(der < length);
	assert(izq >= 0);

	int piv;

	if(izq < der) {
		piv = pivot(a, length, izq, der, ascending, random_pivot);
		quick_sort_recursive(a, length, izq, piv - 1, ascending, random_pivot);
		quick_sort_recursive(a, length, piv + 1, der, ascending, random_pivot);
	}
}

int pivot(int* a, int length, int izq, int der, bool ascending, bool random_pivot) {
	assert(izq <= der);

	int pivot, i = izq + 1, j = der;
	
	if (random_pivot) {
		/*
		long int r = random();
		double ratio = ((double) r) / RAND_MAX;
		double fpivot = ratio * (der - izq) + izq;
		pivot = fpivot;
	
		printf("izq: %d\tder: %d\trand: %ld\tratio: %.12g\tfpivot: %.12g\tpivot: %d\tpivot val: %d\n", izq, der, r, ratio, fpivot, pivot, a[pivot]);
		*/
	
		//pivot = (random() % (der - izq + 1)) + izq; // this could work but it yields poor "random" values (on subpar implementations?); also what if (der - izq + 1) > RAND_MAX?
		pivot = ((double) random()) / RAND_MAX * (der - izq) + izq;

		assert(izq <= pivot && pivot <= der);

		swap(a, length, pivot, izq);
	}
	
	pivot = izq; // follow the pivot!

	while (i <= j) {
		if (!compare(a[pivot], a[i], ascending)) {
			i++;
		} else if (compare(a[pivot], a[j], ascending)) {
				j--;
		} else {
				swap(a, length, i, j);
				i++;
				j--;
		}
	}

	swap(a, length, pivot, j);
	pivot = j; // follow the pivot!

	return pivot;
}


