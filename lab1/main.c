/* First, the standard lib includes, alphabetically ordered */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
/* Then, this project's includes, alphabetically ordered */
#include "array_helpers.h"
#include "sort.h"


// TODO: Why the hell don't we use constants?
#define SELECTION_SORT 's'
#define INSERTION_SORT 'i'
#define BUBBLE_SORT 'b'
#define QUICK_SORT 'q'
#define QUICK_SORT_RANDOM 'r'
#define EXIT 'e'

#define ASC_ORDER 'a'
#define DESC_ORDER 'd'

void print_help(char *program_name) {
    /* Print the usage help of this program. */
    printf("Usage: %s <input file path>\n\n"
           "The input file must have the following format:\n"
           " * The first line must contain only a positive integer, "
           "which is the length of the array.\n"
           " * The second line must contain the members of the array separated "
           "by one or more spaces. Each member must be an integer.\n\n"
           "In other words, the file format is:\n"
           "<amount of array elements>\n"
           "<array elem 1> <array elem 2> ... <array elem N>\n\n",
           program_name);
}

char *parse_filepath(int argc, char *argv[]) {
    /* Parse the filepath given by command line argument. */
    char *result = NULL;

    if (argc < 2) {
        print_help(argv[0]);
        exit(EXIT_FAILURE);
    }

    result = argv[1];

    return(result);
}

/**
 * TODO: if you call flush_stdin when the input buffer is already empty, then the
 * user will be prompted for more chars
 */
static inline void flush_stdin(void) {
	int c;
	while (!feof(stdin) && (c = getchar()) != EOF && c != '\n');
}

/*
#include <sys/select.h>

static inline void flush_stdin2(void) {
	struct timeval tv = { 0L, 0L };
	fd_set readfds;
	FD_ZERO(&readfds);
	FD_SET(0, &readfds); // 0 == fileno(stdin)

	int res = select(1, &readfds, NULL, NULL, &tv);

	printf("select returned %d\n", res);

	if (res == -1) {
		printf("pselect erroed out");
		return;
	} else if (!FD_ISSET(0, &readfds)) {
		printf("buffer seems empty");
		return;
	}

	printf("buffer seems non-empty, draining it...\n");

	int c;
	while (!feof(stdin) && (c = getchar()) != EOF && c != '\n');
}
*/

/*
// this could be used instead of scanf to get the option char from the user
int get_first_char_from_input(void) {
	int c, first;

	// get and save the first character
	first = c = getchar();

	// if it was not newline, keep getting more (but stop on EOF)
	while (c != '\n' && c != EOF)
		c = getchar();

	// in any case, return the first one, which may be '\n' or EOF
	return first;
}
*/

char main_menu(void) {
    char result = '\0';
    int scanf_result = 0;

    printf("Choose the sorting algorithm. Options are:\n"
           "\ts - selection sort\n"
           "\ti - insertion sort\n"
           "\tb - bubble sort\n"
           "\tq - quick sort\n"
           "\tr - quick sort with random pivot\n"
           "\te - exit this program\n"
           "Please enter your choice: ");

    scanf_result = scanf("%c", &result);
    assert(scanf_result == 1);
	flush_stdin();
	//scanf("%*c"); // will discard only one character
    //__fpurge(stdin); // in stdio_ext.h

    return(result);
}

char order_menu(void) {
    char result = '\0';
    int scanf_result = 0;
    
	printf("Choose sorting order. Options are:\n"
		   "\ta - ascending order\n"
		   "\td - descending order\n"
		   "\tany other - cancel and go back to previous menu\n"
		   "Please enter your choice: ");

	scanf_result = scanf("%c", &result);
	assert(scanf_result == 1);
	flush_stdin();
	//scanf("%*c"); // will discard only one character
    //__fpurge(stdin); // in stdio_ext.h

    return(result);
}

bool is_valid_main_option(char option) {
    bool result = false;

    result = (option == SELECTION_SORT ||
              option == INSERTION_SORT ||
              option == BUBBLE_SORT ||
              option == QUICK_SORT ||
              option == QUICK_SORT_RANDOM ||
              option == EXIT);

    return(result);
}

bool is_valid_order_option(char option) {
	bool result = false;
	
	result = option == ASC_ORDER || option == DESC_ORDER;
	
	return result;
}

int main(int argc, char *argv[]) {
    char *filepath = NULL;
    int length = 0;
    char option_main = 0, option_order = 0;
    int *array = NULL, *original_array = NULL;

    /* parse the filepath given in command line arguments */
    filepath = parse_filepath(argc, argv);

    /* parse the array given in the input file */
    length = array_length_from_file(filepath);
    array = array_from_file(filepath, length);

    /* duplicate 'array' so later we can check the ordered array is a
       permutation of the original array */
    original_array = array_duplicate(array, length);

    /* print a simple menu and do the actual sorting */
    do {
        option_main = main_menu();
        switch (option_main) {
            case SELECTION_SORT:
				option_order = order_menu();
            	if (!is_valid_order_option(option_order)) {
            		option_main = 0; // make it invalid so that it loops back
					continue;
				}
                selection_sort(array, length, option_order == ASC_ORDER);
                break;
            case INSERTION_SORT:
				option_order = order_menu();
            	if (!is_valid_order_option(option_order)) {
            		option_main = 0; // make it invalid so that it loops back
					continue;
				}
                insertion_sort(array, length, option_order == ASC_ORDER);
                break;
            case BUBBLE_SORT:
				option_order = order_menu();
            	if (!is_valid_order_option(option_order)) {
            		option_main = 0; // make it invalid so that it loops back
					continue;
				}
                bubble_sort(array, length, option_order == ASC_ORDER);
            	break;
            case QUICK_SORT:
            	option_order = order_menu();
            	if (!is_valid_order_option(option_order)) {
            		option_main = 0; // make it invalid so that it loops back
					continue;
				}
                quick_sort(array, length, option_order == ASC_ORDER, false);
                break;
            case QUICK_SORT_RANDOM:
            	option_order = order_menu();
            	if (!is_valid_order_option(option_order)) {
            		option_main = 0; // make it invalid so that it loops back
					continue;
				}
                quick_sort(array, length, option_order == ASC_ORDER, true);
                break;
            case EXIT:
                printf("Exiting.\n");
                array_destroy(array);
    			array_destroy(original_array);
	            return(EXIT_SUCCESS);
			default:
			    printf("\n\"%c\" is invalid. Please choose a valid "
			           "option.\n\n", option_main);
		}
	} while (!is_valid_main_option(option_main));

    /* check that 'array' not only is sorted, but also it was not changed */
    assert(array_is_permutation_of(original_array, array, length));

    /* show the ordered array in the screen */
    array_dump(array, length);

	// show the quantity of comparisons and swaps:
	printf("Number of comparisons: %d\nNumber of swaps: %d\n", get_comps(), get_swaps());

    /* destroy both arrays */
    array_destroy(array);
    array_destroy(original_array);

    return(EXIT_SUCCESS);
}
