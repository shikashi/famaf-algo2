#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "array_helpers.h"


static bool file_exists(const char *filepath) {
    bool result = false;
    FILE *file = NULL;

    file = fopen(filepath, "r");

    result = (file != NULL);
    if (result) {
        fclose(file);
    }

    return(result);
}

bool array_is_sorted(int *a, int length, bool ascending) {
    bool result = true;
    int i = 0, end = length - 1, add = 1;

    if (!ascending) {
    	i = length - 1; //i = end
    	end = 0;
    	add = -1;
    }
    
    for (; i != end; i += add) {
    	result = result && (a[i] <= a[i+add]);
    }


/*	if (ascending)
		for (i = 0; i < (length - 1); i++) { // for (i = 0; i < (length - 1) && result; i++) {
		    result = result && (a[i] <= a[i+1]);
		}
	else
		for (i = 0; i < (length - 1); i++) { // for (i = 0; i < (length - 1) && result; i++) {
		    result = result && (a[i] >= a[i+1]);
		}
*/

    return(result);
}

bool array_is_equal(int *a, int *other, int length) {
    bool result = true;
    int i = 0;

    for (i = 0; i < length; i++) { // for (i = 0; i < length && result; i++) {
        result = result && (a[i] == other[i]);
    }

    return(result);
}

bool array_has_value(int *a, int value, int length) {
    bool result = false;
    int i = 0;

    for (i = 0; i < length; i++) { // for (i = 0; i < length && !result; i++) {
        result = result || (a[i] == value);
    }

    return(result);
}

int array_value_count(int *a, int length, int value) {
    int i = 0, result = 0;

    for (i = 0; i < length; i++) {
        if (a[i] == value) {
            result += 1;
        }
    }

    return(result);
}

bool array_is_permutation_of(int *a, int *other, int length) {
    bool result = true;
    int i = 0, a_count = 0, other_count = 0;

    for (i = 0; i < length; i++) { // for (i = 0; i < length && result; i++) {
        /* a[i] is also present in 'other' */
        result = result && array_has_value(other, a[i], length);

        /* ocurrences of a[i] in 'a' match those in 'other' */
        a_count = array_value_count(a, length, a[i]);
        other_count = array_value_count(other, length, a[i]);
        result = result && (a_count == other_count);

    }

    return(result);
}

int *array_create(int length) {
    int *result = NULL;

    result = calloc(length, sizeof(int));
    assert(result != NULL);

    return(result);
}

void array_destroy(int *a) {
    free(a);
}

void array_dump(int *a, int length) {
    int i = 0;

    printf("%i\n", length);
    for (i = 0; i < length; i++) {
        printf("%i", a[i]);
        if (i < length - 1) {
            printf(" ");
        } else {
            printf("\n");
        }
    }
}

int *array_duplicate(int *a, int length) {
    int i = 0;
    int *result = NULL;

    result = array_create(length);
    for (i = 0; i < length; i++) {
        result[i] = a[i];
    }

    assert(array_is_equal(a, result, length));

    return(result);
}

int array_length_from_file(const char *filepath) {
    int result = 0, fscanf_result = 0;
    FILE *file = NULL;

    /* Check preconditions */
    assert(file_exists(filepath));

    file = fopen(filepath, "r");
    assert(file != NULL);

    fscanf_result = fscanf(file, "%i\n", &result);

    fclose(file);

    if (fscanf_result != 1 || result < 0) {
        printf("The file does not have the correct format for the array "
               "length.\n");
        exit(EXIT_FAILURE);
    }

    assert(result >= 0);

    return(result);
}

int *array_from_file(const char *filepath, int length) {
    int i = 0, other_length = 0, fscanf_result = 0;
    int *result = NULL;
    FILE *file = NULL;

    /* Check preconditions */
    assert(file_exists(filepath));

    result = array_create(length);

    file = fopen(filepath, "r");
    assert(file != NULL);

    fscanf_result = fscanf(file, "%i\n", &other_length);
    if (fscanf_result != 1 || other_length < 0) {
        printf("The file does not have the correct format for the array "
               "length.\n");
        exit(EXIT_FAILURE);
    }

    if (length != other_length) {
        printf("The array length in the file (%i) does not match the given "
               "length (%i).\n", other_length, length);
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < length; i++) {
        fscanf_result = fscanf(file, "%i", &(result[i]));
        if (fscanf_result != 1) {
            printf("The file does not have the correct format for the array "
                   "elements.\n");
            exit(EXIT_FAILURE);
        }
    }

    fclose(file);

    return(result);
}
