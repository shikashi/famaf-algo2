#ifndef _DICT_H
#define _DICT_H

#include <stdbool.h>
#include "linked_list.h"


typedef char *word_t;
typedef char *def_t;

typedef struct _dict_t *dict_t;

dict_t dict_empty(void);
/*
 * Return a newly created, empty dictionary.
 *
 * The caller must call dict_destroy when done using the resulting dict,
 * so the resources allocated by this call are freed.
 *
 * POST: the result will not be NULL, and dict_length(result) will be 0.
 */

unsigned int dict_length(dict_t dict);
/*
 * Return the amount of entries in the given 'dict'.
 *
 * PRE: the 'dict' must not be NULL.
 */

dict_t dict_add(dict_t dict, word_t word, def_t def);
/*
 * Return a dictionary equals to 'dict' with the given ('word', 'def') added.
 *
 * The given 'word' and 'def' are inserted in the dict, so do not destroy them.
 *
 * PRE: all 'dict', 'word' and 'def' must be not NULL, and 'word' does not
 * exist in the given 'dict'.
 *
 * POST: the length of the result will be the same as the length of 'dict'
 * plus one. The elements of the result will be the same as the one in 'dict'
 * with the new pair ('word', 'def') added.
 */

bool dict_exists(dict_t dict, word_t word);
/*
 * Return if the given 'word' exists in the dictionary 'dict'.
 *
 * PRE: both 'dict' and 'word' must not be NULL.
 */

dict_t dict_remove(dict_t dict, word_t word);
/*
 * Return a dictionary equals to 'dict' with the given 'word' removed.
 *
 * Please note that 'word' may not be in the dictionary (thus an unchanged
 * dict is returned).
 *
 * PRE: both 'dict' and 'word' must not be NULL.
 *
 * POST: the length of the result will be the same as the length of 'dict'
 * minus one if 'word' existed in 'dict'. The elements of the result will be
 * the same as the one in 'dict' with the entry for'word' removed.
 */

def_t dict_search(dict_t dict, word_t word);
/*
 * Return the definition associated with 'word'.
 *
 * The caller must free the resources allocated for the result when done
 * using it.
 *
 * PRE: both 'dict' and 'word' must not be NULL, and 'word' must exist in
 * 'dict'.
 *
 * POST: the result will not be NULL.
 */

dict_t dict_destroy(dict_t dict);
/*
 * Free the resources allocated for the given 'dict', and set it to NULL.
 *
 * PRE: 'dict' must not be NULL.
 */

linked_list_t dict_to_linked_list(dict_t dict);
/*
 * Return a sequence representation of 'dict'.
 *
 * PRE: 'dict' must not be NULL.
 *
 * POST: the result will not be NULL, and the result's length will be the same
 * as the given dict's length. Every entry (word, def) in the dict will be in
 * the returned list.
 */

void dict_dump(dict_t dict);
/*
 * Print the given 'dict' in the standard output.
 *
 * PRE: 'dict' must not be NULL.
 */

#endif
