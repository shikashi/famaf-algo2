#include <assert.h>
#include <stdlib.h>

#include "bst.h"
#include "tree.h"
#include "tuple.h"

/*
* Binary Search Tree implementation
*/

struct _bst_t {
	tree_t root;
	unsigned int count;
};

bst_t bst_empty(void) {
	bst_t bst = calloc(1, sizeof(struct _bst_t));
	assert(bst != NULL);

	bst->root = EMPTY_TREE;
	bst->count = 0;

	return(bst);
}

bst_t bst_destroy(bst_t bst) {
	assert(bst != NULL);
	
	tree_destroy(bst->root);
	free(bst);
	bst = NULL;

	return(bst);
}

bst_t bst_add(bst_t bst, index_t index, data_t data) {
	// check preconditions
	assert(bst != NULL);
	assert(index != NULL);
	assert(data != NULL);
	assert(bst_search(bst, index) == NULL); // TODO: use tree_search()?

	unsigned int old_length = bst->count;

	bst->root = tree_add(bst->root, tuple_from_index_data(index, data));
	bst->count++;

	// check postconditions
	assert(old_length + 1 == bst_length(bst)); // useless, pffffffff

	return(bst);
}

bst_t bst_remove(bst_t bst, index_t index) {
	// check preconditions
	assert(bst != NULL);
	assert(index != NULL);

	if (bst_search(bst, index) != NULL) { // TODO: use tree_search()?
		bst->root = tree_remove(bst->root, index);
		bst->count--;
	}

	return(bst);
}

unsigned int bst_length(bst_t bst) {
	// check preconditions
	assert(bst != NULL);

	return(bst->count);
}

bst_t bst_copy(bst_t bst) {
	// check preconditions
	assert(bst != NULL);

	bst_t copy = bst_empty();
	copy->root = tree_copy(bst->root);
	copy->count = bst->count;

	return(copy);
}

data_t bst_search(bst_t bst, index_t index) {
	// check preconditions
	assert(bst != NULL);
	assert(index != NULL);

	return(tree_search(bst->root, index));
}

linked_list_t bst_to_linked_list(bst_t bst) {
	// check preconditions
	assert(bst != NULL);

	return(tree_flatten_inorder(bst->root, list_empty()));
}

void bst_dump(bst_t bst, FILE *fd) {
	// check preconditions
	assert(bst != NULL);

	linked_list_t bst_as_list = bst_to_linked_list(bst);
	list_dump(bst_as_list, fd);
	list_destroy(bst_as_list);
}
