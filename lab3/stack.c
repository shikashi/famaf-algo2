#include <assert.h>
#include <stdlib.h>

#include "stack.h"

typedef struct _stack_node_t* stack_node_t;

struct _stack_node_t {
	void* data;
	stack_node_t next;
};

stack_t stack_create(void) {
	return(NULL);
}

stack_t stack_push(stack_t stack, void* data) {
	stack_node_t n = calloc(1, sizeof(struct _stack_node_t));
	assert(n != NULL);
	n->data = data;
	n->next = stack;
	return(n);
}

stack_t stack_pop(stack_t stack) {
	assert(stack != NULL);
	
	stack_node_t next = stack->next;
	free(stack);

	return(next);
}

void* stack_top(stack_t stack) {
	assert(stack != NULL);
	
	return(stack->data);
}

bool stack_empty(stack_t stack) {
	return(stack == NULL);
}

stack_t stack_destroy(stack_t stack) {
	stack_node_t n = stack;
	while (n != NULL) {
		stack_node_t m = n->next;
		free(n);
		n = m;
	}
	
	return(NULL);
}
