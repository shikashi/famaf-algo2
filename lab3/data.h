#ifndef _DATA_H
#define _DATA_H

#include <stdbool.h>


typedef struct _data_t *data_t;

unsigned int data_max_length(void);
/*
 * Return the maximum length for the string representation of any data.
 */

unsigned int data_length(data_t data);
/*
 * Return the length of the given 'data'.
 *
 * PRE: 'data' must be not NULL.
 */

char *data_to_string(data_t data);
/*
 * Return the string representation of the given 'data'.
 *
 * The caller is responsible for the allocated reources for the result, thus
 * those should be freed when done using it.
 *
 * PRE: 'data' must be not NULL.
 *
 * POST: the result will be a not NULL, null-terminated string.
 */

data_t data_from_string(char *source);
/*
 * Return a newly created data from the given string 'source'.
 *
 * The string 'source' must be null-terminated, and this function will
 * make a copy of it, so is the caller's responsability to free it.
 *
 * PRE: 'source' must be not NULL.
 *
 * POST: the result will be not NULL, and the string representation of it
 * will be equal to 'source'.
 */

data_t data_copy(data_t data);
/*
 * Return a cloned copy of the given 'data'.
 *
 * PRE: 'data' must be not NULL.
 *
 * POST: the result will be not NULL, and the string representation of it
 * will be equal to the string representation of 'data'.
 */

data_t data_destroy(data_t data);
/*
 * Free the resources used by the given 'data', and set it to NULL.
 */

#endif
