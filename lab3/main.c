#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "dict.h"
#include "helpers.h"

#define OPTION_SIZE 'z'
#define OPTION_SEARCH 's'
#define OPTION_ADD 'a'
#define OPTION_DELETE 'd'
#define OPTION_EMPTY 'e'
#define OPTION_PRINT 'p'
#define OPTION_LOAD 'l'
#define OPTION_DUMP 'u'
#define OPTION_QUIT 'q'

static inline char* readline_or_die(void) {
    char* str = readline_from_stdin();
    if (str == NULL) {
    	printf("ERROR: Error occured while attempting to read from standard input. Exiting.\n");
    	exit(EXIT_FAILURE);
    }
    return(str);
}

char main_menu(void) {
	printf("Choose. Options are:\n\n");
    printf( "\t/**************************************\\\n"
    		"\t* z: Size of the dictionary            *\n"
			"\t* s: Search for an entry in the dict   *\n"
			"\t* a: Add a new entry to the dict       *\n"
			"\t* d: Delete an entry from the dict     *\n"
			"\t* e: Empty the dict                    *\n"
			"\t* p: Print the dict to stdout          *\n"
			"\t* l: Load the dict from a file         *\n"
			"\t* u: Dump the dict to a file           *\n"
			"\t* q: Quit                              *\n"
			"\t\\**************************************/\n"
			"\n\tPlease enter your choice: ");

    char* str = readline_or_die();
	char result = str[0];
	free(str);

	return(result);
}

//int main(int argc, char** argv) {
int main(void) {

	dict_t dictionary = dict_empty();
	word_t word = NULL;
	def_t def = NULL;
	char* path = NULL;
	char opt;

	while (true) {
		switch (opt = main_menu()) {
			case OPTION_SIZE:
				printf("\t\tDictionary size: %u\n\n", dict_length(dictionary));
				break;
			case OPTION_SEARCH:
				printf("\t\tWord to look for: ");
				word = readline_or_die();
				if (dict_exists(dictionary, word)) {
					def = dict_search(dictionary, word);
					printf("\t\t\t%s:\n\t\t\t%s\n", word, def);
					free(def);
				} else {
					printf("\t\tThe word  \"%s\" does not exist in the dictionary.\n\n", word);
				}
				free(word);
				break;
			case OPTION_ADD:
				printf("\t\tType the word: ");
				word = readline_or_die();
				if (dict_exists(dictionary, word)) {
					printf("\t\tWord \"%s\" already exists.\n", word);
				} else {
					printf("\t\tType its definition: ");
					def = readline_or_die();
					dictionary = dict_add(dictionary, word, def);
					free(def);
					printf("\t\tEntry added successfully.\n\n");
				}
				free(word);
				break;
			case OPTION_DELETE:
				printf("\t\tType the word: ");
				word = readline_or_die();
				dict_remove(dictionary, word);
				printf("\t\tThe word \"%s\" was deleted succesfully.\n", word);
				free(word);
				break;
			case OPTION_EMPTY:
				dict_destroy(dictionary);
				dictionary = dict_empty();
				printf("\t\tDictionary cleared.\n\n");
				break;
			case OPTION_PRINT:
				dict_dump(dictionary);
				break;
			case OPTION_LOAD:
				printf("\t\tEnter the path to the file: ");
				path = readline_or_die();
				dict_destroy(dictionary);
				dictionary = dict_from_file(path);
				free(path);
				break;
			case OPTION_DUMP:
				printf("\t\tEnter the path for the new file: ");
				path = readline_or_die();
				dict_to_file(dictionary, path);
//				printf("\t\tThe dictionary was saved in the file succesfully.\n"); // Cannot know
				free(path);
				break;
			case OPTION_QUIT:
				printf("Quiting...\n");
				dict_destroy(dictionary);
				return(EXIT_SUCCESS);
				break;
			default:
				printf("Invalid option. Please try again.\n");
		}

		printf("Press enter to continue...");
		char* tmp = readline_or_die();
		free(tmp);
	}

	dict_destroy(dictionary); // see the next comment // yup, i saw it

	return(EXIT_SUCCESS); // not unreachable code //HA HA! // wait! a break from the while could make it execute // so it's not unreachable // that's right
}
