#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // needed only when debugging (see node_fully_equal)

#include "linked_list.h"
#include "tuple.h"

typedef struct _node_t *node_t;

struct _node_t {
	tuple_t content;
	node_t next;
};

/**
 * Note: Conditions for an empty list are (head == NULL)
 * or (size == 0), although for the latter we need to assume
 * there will be no overflow.
 * Note: If (head == NULL) the value of tail is unspecified.
 */
struct _linked_list_t {
	unsigned size;
	node_t head;
	node_t tail;
};

/**
 * We don't allocate mem for index and data, we just link them
 */
node_t node_from_index_data(index_t i, data_t d) {
	node_t n = calloc(1, sizeof(struct _node_t));
	assert(n != NULL);

	n->content = tuple_from_index_data(i, d);
	n->next = NULL;

	return(n);
}

node_t node_destroy(node_t n) {
	assert(n != NULL);

	tuple_destroy(n->content);
	free(n);

	return(NULL);
}

/**
 * For debugging purpose only.
 */
static inline bool node_fully_equal(node_t node1, node_t node2) {
	assert(node1 != NULL);
	assert(node2 != NULL);

	if (!index_is_equal(tuple_fst(node1->content), tuple_fst(node2->content)))
		return(false);

	char* data1_str = data_to_string(tuple_snd(node1->content));
	char* data2_str = data_to_string(tuple_snd(node2->content));
	bool r = !strcmp(data1_str, data2_str);
	free(data1_str);
	free(data2_str);

	return(r);
}

/**
 * For debugging purpose only.
 */
static inline bool list_equal(linked_list_t list1, linked_list_t list2) {
	assert(list1 != NULL);
	assert(list2 != NULL);

	if (list1->size != list2->size)
		return(false);

	node_t n1 = list1->head, n2 = list2->head;
	for (; n1 != NULL && n2 != NULL; n1 = n1->next, n2 = n2->next)
		if (!node_fully_equal(n1, n2))
			return(false);

	return(n1 == NULL && n2 == NULL); // paranoid: we check that both lists have been fully traversed
}

linked_list_t list_empty(void) {
	linked_list_t list = calloc(1, sizeof(struct _linked_list_t));

	//check postconditions
	assert(list != NULL);

	list->size = 0; // unnecessary, memory is blank
	list->head = NULL;  // unnecessary, memory is blank
	list->tail = NULL;

	return(list);
}

// Note: Using list_add() while copying a sorted list
// would be very inefficient. Do a lower-level copy instead.
linked_list_t list_copy(linked_list_t list) {
	linked_list_t copy = list_empty();	

	node_t* p = &copy->head;
	node_t q = NULL;
	for (node_t node = list->head; node != NULL; node = node->next) {
		node_t new_node = node_from_index_data(index_copy(tuple_fst(node->content)), data_copy(tuple_snd(node->content)));
		q = new_node; // keep track of the last node copied node
		*p = new_node; // make the (head|previous node) point to the newly created node
		p = &new_node->next;
	}
	copy->tail = q;
	
	copy->size = list->size;

	// check postconditions
	assert(copy != NULL);
	assert(list_length(copy) == list_length(list));
	assert(list_equal(list, copy));

	return(copy);
}

unsigned int list_length(linked_list_t list) {

	// check preconditions
	assert(list != NULL);

	return(list->size);
}

// Note: You could perceptibly optimize list_add by
// using a tail pointer in the list and checking whether
// the element to be added is larger than the tail. The
// performance gain would be particularly noticeable when
// a series of elements are added in order to the list.
// We improve from n^2/2 - n/2 (O(n^2)) to n-1 (O(n)) 
// where n is the total amount of elements to be 
// added.
// Notice that if list_copy() is implemented in terms of
// list_add(), the algorithmic complexity of the
// aforementioned function improves as previously
// stated. (Although doing a low-level copy of nodes
// would further reduce list_copy's complexity to 0.)
//
// TODO: Explore about recursive implementations.
linked_list_t list_add(linked_list_t list, index_t index, data_t data) {

	// check preconditions
	assert(list != NULL);
	assert(index != NULL);
	assert(data != NULL);

	unsigned int old_length = list_length(list);

	node_t n = node_from_index_data(index, data);

	if (list->head != NULL && !index_is_less_than(index, tuple_fst(list->tail->content))) { // append the element at the tail
		list->tail->next = n;
		list->tail = n;
	} else { // either list is empty or there is at least one element greater than index
		node_t* p = &list->head;

		// look for the appropriate place for _inserting_ the new node
		while (*p != NULL && index_is_less_than(tuple_fst((*p)->content), index))
			p = &(*p)->next;
		// At the end of this loop (*p) is the pointer to the first entry greater than or equal to index,
		// or NULL if the list is empty
		
		node_t next = *p;
		*p = n;
		n->next = next;
		
		if (next == NULL) // same as list->size == 0, i.e. the list was empty
			list->tail = n;
		// else, we added the element somewhere amid the list, so the tail remains unchanged
	}

	list->size++;

	// check postconditions
	assert(old_length + 1 == list_length(list)); // this is pretty useless, it won't even catch overflows
	// TODO: check entries?

	return(list);
}

// TODO: You can appy here an optimization similar
// to the one in list_add()... But does it worth it?
linked_list_t list_remove(linked_list_t list, index_t index) {

	// check preconditions
	assert(list != NULL);
	assert(index != NULL);

	if (list->size == 0)
		return(list);

	node_t* p = &list->head;
	node_t prev = NULL; // if we remove the last element, we need to know which was the previous one so we can update the tail

	// look for an entry with the given index
	while (*p != NULL && index_is_less_than(tuple_fst((*p)->content), index)) {
		prev = *p;
		p = &(*p)->next;
	}

	if (*p != NULL && index_is_equal(tuple_fst((*p)->content), index)) { // we found the entry
		node_t next = (*p)->next;
		node_destroy(*p);
		*p = next;

		list->size--;

		if (next == NULL) // we removed the last element, therefore we update the tail
			list->tail = prev;
	}
	// else the element was not in the list

	assert(list_search(list, index) == NULL);

	return(list);
}

// TODO: You can appy here an optimization similar
// to the one in list_add()... But does it worth it?
data_t list_search(linked_list_t list, index_t index) {

	// check preconditions
	assert(list != NULL);
	assert(index != NULL);

	node_t node = list->head;

	while (node != NULL && index_is_less_than(tuple_fst(node->content), index))
		node = node->next;

	if (node != NULL && index_is_equal(tuple_fst(node->content), index))
		return(tuple_snd(node->content));
	else
		return(NULL);
}

linked_list_t list_destroy(linked_list_t list) {

	// check preconditions
	assert(list != NULL);

	for (node_t node = list->head; node != NULL; ) {
		node_t next = node->next;
		node_destroy(node);
		node = next;
	}

	free(list);

	return(NULL);
}

void list_dump(linked_list_t list, FILE *fd) {
	for (node_t node = list->head; node != NULL; node = node->next) {
		char* index_str = index_to_string(tuple_fst(node->content));
		char* data_str = data_to_string(tuple_snd(node->content));
		fprintf(fd, "%s: %s\n", index_str, data_str);
		free(index_str);
		free(data_str);
	}
}
