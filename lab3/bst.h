#ifndef _BST_H
#define _BST_H

#include <stdio.h>

#include "data.h"
#include "index.h"
#include "linked_list.h"

typedef struct _bst_t *bst_t;


bst_t bst_empty(void);
/*
* Return a newly created, empty binary search tree (BST).
*
* The caller must call bst_destroy when done using the resulting BST,
* so the resources allocated by this call are freed.
*
* POST: the result will not be NULL, and bst_length(result) will be 0.
*/

bst_t bst_destroy(bst_t bst);
/*
* Free the resources allocated for the given 'bst', and set it to NULL.
*
* PRE: 'bst' must not be NULL.
*/

bst_t bst_add(bst_t bst, index_t index, data_t data);
/*
* Return a BST equal to 'bst' with the ('index', 'data') added to it.
*
* The given 'index' and 'data' are inserted in the BST, so do not destroy
* them.
*
* PRE: all 'bst', 'index' and 'data' must be not NULL. Also, the pair
* ('index', 'data') must not exist in the given BST.
*
* POST: the length of the result will be the same as the length of 'bst'
* plus one. The elements of the result will be the same as the one in 'bst'
* with the new pair ('index', 'data') added acordingly (see:
* http://en.wikipedia.org/wiki/Binary_search_tree
* for specifications about behavior).
*
*/

bst_t bst_remove(bst_t bst, index_t index);
/*
* Return a BST equals to 'bst' with the (index, <data>) tuple
* occurrence removed.
*
* Please note that 'index' may not be in the BST (thus an unchanged
* BST is returned).
*
* PRE: both 'bst' and 'index' must not be NULL.
*
* POST: the length of the result will be the same as the length of 'bst'
* minus one if 'index' existed in 'bst'. The elements of the result will be
* the same as the one in 'bst' with the entry for 'index' removed.
*/

bst_t bst_copy(bst_t bst);
/*
* Return a newly created copy of the given 'bst'.
*
* The caller must call bst_destroy when done using the resulting BST,
* so the resources allocated by this call are freed.
*
* POST: the result will not be NULL and it will be an exact copy of 'bst'.
* In particular, bst_length(result) will be equal to the length of 'bst'.
*/

unsigned int bst_length(bst_t bst);
/*
* Return the number of elements in the given 'bst'.
* Constant order complexity.
*
* PRE: 'bst' must not be NULL.
*/

data_t bst_search(bst_t bst, index_t index);
/*
* Return the data associated to the given 'index' in BST,
* or NULL if the given 'index' is not in 'bst'.
*
* The caller must NOT free the resources allocated for the result when done
* using it.
*
* PRE: both 'bst' and 'index' must not be NULL.
*/

linked_list_t bst_to_linked_list(bst_t bst);
/*
* Return a sequence representation of 'bst'.
*
* PRE: 'bst' must not be NULL.
*
* POST: the result will not be NULL, and the result's length will be the same
* as the given BST's length. Every value (index, data) in the BST will be in
* the returned list, and the BST order should be preserved.
*
* In other words, the resulting list has to be ascendantly ordered.
*
*/

void bst_dump(bst_t bst, FILE *fd);
/*
* Dump the BST using its string representation to the given
* file descriptor.
*
* PRE: 'bst' must not be NULL, and 'fd' must be a valid file descriptor.
*/

#endif
