#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

#include "index.h"
#include "tree.h"

const tree_t EMPTY_TREE = NULL;
//#define EMPTY_TREE ((tree_t) NULL)

struct _tree_t {
	tuple_t value;
	tree_t left;
	tree_t right;
};

tree_t tree_leaf(tuple_t value) {
	tree_t leaf = calloc(1, sizeof(struct _tree_t));

	assert(leaf != NULL);

	leaf->value = value;
	leaf->left = EMPTY_TREE;
	leaf->right = EMPTY_TREE;

	return(leaf);
}

tree_t tree_destroy(tree_t tn) {
	if (tn != EMPTY_TREE) {
		tree_destroy(tn->left);
		tuple_destroy(tn->value); // in this order just for consistency
		tree_destroy(tn->right);
		free(tn);
		tn = EMPTY_TREE;
	}
	return(tn);
}

tree_t tree_add(tree_t tree, tuple_t value) {
	index_t index = tuple_fst(value);
	
	if (tree == EMPTY_TREE)
		tree = tree_leaf(value);
	else {
		index_t this_index = tuple_fst(tree->value);
		if (index_is_less_than(index, this_index))
			tree->left = tree_add(tree->left, value);
		else if (index_is_less_than(this_index, index))
			tree->right = tree_add(tree->right, value);
	}
	
	return(tree);
}

/**
 * tree_extract_max(tree, &out_max)
 * 
 * Given the tree "tree", recursively find the maximum node value, copy it in the argument pointer
 * "out_max" and remove it from the given tree.
 * Return the argument tree without the maximum.
 *
 * PRE: both tree and out_max must not be NULL.
 *
 * TODO: Some optimizing compilers can (partially) inline a recursive function by unrolling
 * a portion of the call stack. But! Here we are modifying the arguments, how would that go?
 *
 */
static inline tree_t tree_extract_max(tree_t tree, tuple_t* out_max) {
	assert(tree != EMPTY_TREE);
	
	if (tree->right == EMPTY_TREE) { // we got the max
		*out_max = tree->value; // we deliver the max
		tree_t left = tree->left; // save the left tree, which might not be empty
		free(tree); // free this node; don't worry the max won't be lost
		tree = left; // the rest of the tree remains
	} else { // keep looking for the max, which will be to the right, of course
		tree->right = tree_extract_max(tree->right, out_max);
	}
	
	return(tree);
}

/**
 * tree_remove(tree, index)
 * 
 * PRE: index must not be NULL.
 */
tree_t tree_remove(tree_t tree, index_t index) {
	assert(index != NULL);

	if (tree != EMPTY_TREE) {
		index_t this_index = tuple_fst(tree->value);
		if (index_is_less_than(index, this_index)) {
			tree->left = tree_remove(tree->left, index);
		} else if (index_is_less_than(this_index, index)) {
			tree->right = tree_remove(tree->right, index);
		} else { // we found the element to be removed
			tuple_destroy(tree->value); // destroy the element
			if (tree->left == EMPTY_TREE) { // this node has at most one child, we lift that child
				tree_t right = tree->right;
				free(tree);
				tree = right;
			} else if (tree->right == EMPTY_TREE) { // this node has at most one child, we lift that child
				tree_t left = tree->left;
				free(tree);
				tree = left;
			} else { // from the left subtree, put the max here and remove it from that left subtree
				tree->left = tree_extract_max(tree->left, &tree->value);
			}
		}
	}

	return(tree);
}

static inline tree_t tree_get_max(tree_t tree) {
	assert(tree != EMPTY_TREE);
	
	if (tree->right != EMPTY_TREE)
		return(tree_get_max(tree->right));
	return(tree);
}

tree_t tree_remove2(tree_t tree, index_t index) {
	assert(index != NULL);
	
	if (tree != EMPTY_TREE) {
		index_t this_index = tuple_fst(tree->value);
		if (index_is_less_than(index, this_index)) {
			tree->left = tree_remove(tree->left, index);
		} else if (index_is_less_than(this_index, index)) {
			tree->right = tree_remove(tree->right, index);
		} else { // we found the element to be removed
			tuple_destroy(tree->value); // destroy the element
			if (tree->left == EMPTY_TREE) { // this node has at most one child, we lift that child
				tree_t right = tree->right;
				free(tree);
				tree = right;
			} else if (tree->right == EMPTY_TREE) { // this node has at most one child, we lift that child
				tree_t left = tree->left;
				free(tree);
				tree = left;
			} else { // from the left subtree, put the max here and remove it from that left subtree
				tree_t max_tree = tree_get_max(tree->left); // get the subtree which root is maximum
				tree->value = tuple_copy(max_tree->value); // put that max here
				tree->left = tree_remove2(max_tree, tuple_fst(max_tree->value)); // remove that max
			}
		}
	}

	return(tree);
}


tree_t tree_copy(tree_t tree) {
	tree_t copy = EMPTY_TREE;
	if (tree != EMPTY_TREE) {
		copy = tree_leaf(tuple_copy(tree->value));
		copy->left = tree_copy(tree->left);
		copy->right = tree_copy(tree->right);
	}

	return(copy);
}

data_t tree_search(tree_t tree, index_t index) {
	assert(index != NULL);
	
	if (tree == EMPTY_TREE)
		return(NULL);
	else {
		index_t this_index = tuple_fst(tree->value);
		
		if (index_is_less_than(index, this_index))
			return(tree_search(tree->left, index));
		else if (index_is_less_than(this_index, index))
			return(tree_search(tree->right, index));
		else // we found the entry
			return(tuple_snd(tree->value));
	}
}

/**
 * tree_flatten(tree, list)
 * 
 * Flatten tree, in-order style.
 */
linked_list_t tree_flatten_inorder(tree_t tree, linked_list_t list) {
	assert(list != NULL);

	if (tree != EMPTY_TREE) { // adding in this order would make it work even with an unsorted list as backend
		list = tree_flatten_inorder(tree->left, list);
		list = list_add(list, index_copy(tuple_fst(tree->value)), data_copy(tuple_snd(tree->value)));
		list = tree_flatten_inorder(tree->right, list);
	}

	return(list);
}

/**
 * tree_flatten(tree, list)
 * 
 * Flatten tree, pre-order style.
 */
linked_list_t tree_flatten_preorder(tree_t tree, linked_list_t list) {
	assert(list != NULL);

	if (tree != EMPTY_TREE) { // adding in this order would make it work even with an unsorted list as backend
		list = list_add(list, index_copy(tuple_fst(tree->value)), data_copy(tuple_snd(tree->value)));
		list = tree_flatten_preorder(tree->left, list);
		list = tree_flatten_preorder(tree->right, list);
	}

	return(list);
}

/**
 * tree_flatten(tree, list)
 * 
 * Flatten tree, post-order style.
 */
linked_list_t tree_flatten_postorder(tree_t tree, linked_list_t list) {
	assert(list != NULL);

	if (tree != EMPTY_TREE) { // adding in this order would make it work even with an unsorted list as backend
		list = tree_flatten_postorder(tree->left, list);
		list = tree_flatten_postorder(tree->right, list);
		list = list_add(list, index_copy(tuple_fst(tree->value)), data_copy(tuple_snd(tree->value)));
	}

	return(list);
}
