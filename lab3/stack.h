#ifndef _STACK_H
#define _STACK_H

#include <stdbool.h>

typedef struct _stack_node_t* stack_t;

stack_t stack_create(void);

stack_t stack_push(stack_t stack, void* data);

stack_t stack_pop(stack_t stack);

void* stack_top(stack_t stack);

bool stack_empty(stack_t stack);

stack_t stack_destroy(stack_t stack);

#endif

