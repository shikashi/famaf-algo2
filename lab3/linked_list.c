#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // needed only when debugging (see node_fully_equal)

#include "linked_list.h"
#include "tuple.h"

typedef struct _node_t *node_t;

struct _node_t {
	tuple_t content;
	node_t next;
};

struct _linked_list_t {
	unsigned size;
	node_t head;
	node_t tail;
};

/**
 * We don't allocate mem for the tuple, we just link it
 */
/*node_t node_from_tuple(tuple_t t) {
	node_t n = calloc(1, sizeof(struct _node_t));
	n->content = t;
	n->next = NULL;
	return(n);
}*/

/**
 * We don't allocate mem for index and data, we just link them
 */
node_t node_from_index_data(index_t i, data_t d) {
	node_t n = calloc(1, sizeof(struct _node_t));
	n->content = tuple_from_index_data(i, d);
	n->next = NULL;
	return(n);
}

node_t node_destroy(node_t n) {
	tuple_destroy(n->content);
	free(n);
	return(NULL);
}

/*
node_t node_copy(node_t node) {
	node_t copy = calloc(1, sizeof(struct _node_t))

	return(copy);
	return(node_from_tuple(node->content));
}
*/

/**
 * For debug purpose only.
 */
static inline bool node_fully_equal(node_t node1, node_t node2) {
	assert(node1 != NULL);
	assert(node2 != NULL);

	if (!index_is_equal(tuple_fst(node1->content), tuple_fst(node2->content)))
		return(false);

	char* data1_str = data_to_string(tuple_snd(node1->content));
	char* data2_str = data_to_string(tuple_snd(node2->content));
	bool r = !strcmp(data1_str, data2_str);
	free(data1_str);
	free(data2_str);
	return(r);
}

/**
 * For debug purpose only.
 */
static inline bool list_equal(linked_list_t list1, linked_list_t list2) {
	assert(list1 != NULL);
	assert(list2 != NULL);

	if (list1->size != list2->size)
		return(false);

	node_t n1 = list1->head, n2 = list2->head;
	for (; n1 != NULL && n2 != NULL; n1 = n1->next, n2 = n2->next)
		if (!node_fully_equal(n1, n2))
			return(false);

	return(n1 == NULL && n2 == NULL); // paranoid: we check that both lists have been fully traversed
}

linked_list_t list_empty(void) {
	linked_list_t list = calloc(1, sizeof(struct _linked_list_t));

	//check postconditions
	assert(list != NULL);

	list->size = 0; // unnecessary, memory is blank
	list->head = NULL;  // unnecessary, memory is blank
	list->tail = NULL;

	return(list);
}

linked_list_t list_copy(linked_list_t list) {
	linked_list_t copy = list_empty();
	
	for (node_t node = list->head; node != NULL; node = node->next)
		copy = list_add(copy, index_copy(tuple_fst(node->content)), data_copy(tuple_snd(node->content)));

	// check postconditions
	assert(copy != NULL);
	assert(list_length(copy) == list_length(list));
	assert(list_equal(list, copy));

	return(copy);
}

unsigned int list_length(linked_list_t list) {

	// check preconditions
	assert(list != NULL);

	return(list->size);
}

linked_list_t list_add(linked_list_t list, index_t index, data_t data) {

	// check preconditions
	assert(list != NULL);
	assert(index != NULL);
	assert(data != NULL);

	unsigned int old_length = list_length(list);

	node_t n = node_from_index_data(index, data);

	if (list->head == NULL)
		list->head = n;
	else
		list->tail->next = n;
	list->tail = n;

	list->size++;

	// check postconditions
	assert(old_length + 1 == list_length(list));
	// TODO: check entries? 

	return(list);
}

linked_list_t list_remove(linked_list_t list, index_t index) {

	// check preconditions
	assert(list != NULL);
	assert(index != NULL);

	if (list->size == 0)
		return(list);

	node_t* p = &list->head;
	node_t prev = NULL; // we need this if we are going to use a tail pointer

	// look for an entry with the given index
	while (*p != NULL && !index_is_equal(tuple_fst((*p)->content), index)) {
		prev = *p;
		p = &(*p)->next;
	}

	if (*p != NULL) { // we found the entry
		node_t next = (*p)->next;
		node_destroy(*p);
		*p = next;

		list->size--;

		if (next == NULL) // we removed the last entry, therefore we update the tail
			list->tail = prev;
	}

	assert(list_search(list, index) == NULL);

	return(list);
}

data_t list_search(linked_list_t list, index_t index) {

	// check preconditions
	assert(list != NULL);
	assert(index != NULL);

	for (node_t node = list->head; node != NULL; node = node->next)
		if (index_is_equal(tuple_fst(node->content), index))
			return(tuple_snd(node->content));

	return(NULL);
}

linked_list_t list_destroy(linked_list_t list) {

	// check preconditions
	assert(list != NULL);

	for (node_t node = list->head; node != NULL; ) {
		node_t next = node->next;
		node_destroy(node);
		node = next;
	}

	free(list);

	return(NULL);
}

void list_dump(linked_list_t list, FILE *fd) {
	for (node_t node = list->head; node != NULL; node = node->next) {
		char* index_str = index_to_string(tuple_fst(node->content));
		char* data_str = data_to_string(tuple_snd(node->content));
		fprintf(fd, "%s: %s\n", index_str, data_str);
		free(index_str);
		free(data_str);
	}
}
