#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "tuple.h"

struct _tuple_t {
	index_t index;
	data_t data;
};

tuple_t tuple_from_index_data(index_t index, data_t data) {

	// check preconditions
	assert(index != NULL);
	//assert(data != NULL);

	tuple_t t = calloc(1, sizeof(struct _tuple_t));

	t->index = index;
	t->data = data;

	return(t);
}

tuple_t tuple_copy(tuple_t t) {

	// check preconditions
	assert(t != NULL);

	tuple_t copy = tuple_from_index_data(index_copy(t->index), data_copy(t->data));

	return(copy);
}

tuple_t tuple_destroy(tuple_t t) {

	assert(t != NULL);

	index_destroy(t->index);
	data_destroy(t->data);
	free(t);

	return(NULL);
}

index_t tuple_fst(tuple_t t) {

	// check preconditions
	assert(t != NULL);

	return(t->index);
}

data_t tuple_snd(tuple_t t) {

	// check preconditions
	assert(t != NULL);

	return(t->data);
}

