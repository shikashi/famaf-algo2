#ifndef _STACK_H
#define _STACK_H

#include <stdbool.h>

#include "tree.h"

//
// Not the same kind of stack used to store edges.
// We are going to use this stack to store tree_t and to implement
// things like tree_destroy(), so destroying the elements on a
// stack_pop() means using tree_destroy() (!).
// Therefore, we don't destroy elements on pop and don't return
// copies on top.
//

/* Stack element abstract data type */

//typedef tree_t stack_elem_t;
typedef void* stack_elem_t;

/* Stack abstract data type */

typedef struct _node_t *stack_t;

stack_t stack_empty(void);

stack_t stack_destroy(stack_t stack);

stack_t stack_push(stack_t stack, stack_elem_t elem);

stack_t stack_pop(stack_t stack);

stack_elem_t stack_top(stack_t stack);

bool stack_is_empty(stack_t stack);

#endif
