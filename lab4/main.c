#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "graph.h"
#include "helpers.h"
#include "priority_queue.h"
//#include "stack_edge.h"    // the use of these stacks is completely unnecessary
#include "union_find.h"

graph_t kruskal(graph_t input_graph, FILE* fd) {
	unsigned nedges = edges_count(input_graph);
	unsigned nvertices = vertices_count(input_graph);
//	stack_t mst_edges = stack_empty();  // unused
//	stack_t unused_edges = stack_empty();   // unused

	priority_queue_t pqueue = priority_queue_empty(nedges);
	union_find_t ufind = union_find_create(nvertices);

	edge_t* edges = graph_edges(input_graph);
	for (unsigned i = 0; i < nedges; i++) {
		pqueue = priority_queue_enqueue(pqueue, edges[i]);
//		priority_queue_dump(pqueue, stdout); // DEBUG
		ufind = union_find_push(ufind, vertex_label(edge_left_vertex(edges[i])));
		ufind = union_find_push(ufind, vertex_label(edge_right_vertex(edges[i])));
	}
	free(edges);

	graph_t mst_graph = graph_empty(graph_name(input_graph), vertices_count(input_graph));

	while (union_find_class_count(ufind) > 1 && !priority_queue_is_empty(pqueue)) {
		edge_t min_edge = priority_queue_first(pqueue);
//		printf("selected edge: %d", edge_weight(min_edge)); // DEBUG
		pqueue = priority_queue_dequeue(pqueue);
		union_find_class_t class1 = union_find_find(ufind, vertex_label(edge_left_vertex(min_edge)));
		union_find_class_t class2 = union_find_find(ufind, vertex_label(edge_right_vertex(min_edge)));
		if (class1 != class2) {
			ufind = union_find_union(ufind, class1, class2);
			mst_graph = graph_add_edge(mst_graph, edge_set_primary(min_edge, true));
//			mst_edges = stack_push(mst_edges, edge_set_primary(min_edge, true));    // unused
//			printf(" (used)\n"); // DEBUG
		} else {
			mst_graph = graph_add_edge(mst_graph, edge_set_primary(min_edge, false));
//			unused_edges = stack_push(unused_edges, edge_set_primary(min_edge, false)); // unused
//			printf(" (unused)\n"); // DEBUG
		}
	}
	while (!priority_queue_is_empty(pqueue)) {
		edge_t min_edge = priority_queue_first(pqueue);
		pqueue = priority_queue_dequeue(pqueue);
		mst_graph = graph_add_edge(mst_graph, edge_set_primary(min_edge, false));
	}

	fprintf(fd, "/* Componentes conexas: %d */\n", union_find_class_count(ufind));

// unused:
//
//	while (!stack_is_empty(mst_edges)) {
//		edge_t edge = stack_top(mst_edges);
//		mst_graph = graph_add_edge(mst_graph, edge);
//		mst_edges = stack_pop(mst_edges);
//	}
//	while (!stack_is_empty(unused_edges)) {
//		edge_t edge = stack_top(unused_edges);
//		mst_graph = graph_add_edge(mst_graph, edge);
//		unused_edges = stack_pop(unused_edges);
//	}

//	stack_destroy(mst_edges);   // unused
//	stack_destroy(unused_edges);    // unused
	union_find_destroy(ufind);
	priority_queue_destroy(pqueue);

	return mst_graph;
}

int main(void) {

	graph_t graph_in = read_graph_from_file(stdin);

	graph_t graph_out = kruskal(graph_in, stdout);

	graph_dump(graph_out, stdout);

	graph_destroy(graph_in);
	graph_destroy(graph_out);

	return EXIT_SUCCESS;
}
