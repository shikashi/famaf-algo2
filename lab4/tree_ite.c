#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

#include "index.h"
#include "stack_tree.h"
#include "tree.h"

const tree_t EMPTY_TREE = NULL;

struct _tree_t {
	tuple_t value;
	tree_t left;
	tree_t right;
};

tree_t tree_leaf(tuple_t value) {
	tree_t leaf = calloc(1, sizeof(struct _tree_t));

	assert(leaf != NULL);

	leaf->value = value;
	leaf->left = EMPTY_TREE;
	leaf->right = EMPTY_TREE;

	return leaf;
}

tree_t tree_destroy(tree_t tree) {

	stack_t stack = stack_empty();
	tree_t curr = tree;
	tree_t prev = NULL;

	// post-order iterative traversal
	while (curr != EMPTY_TREE || !stack_is_empty(stack)) {
		if (curr == EMPTY_TREE) { // no more nodes to the left
			tree_t top = stack_top(stack);
			if (top->right == EMPTY_TREE || top->right == prev) { // nor to the right
				// so deal with this node
				curr = top;
				stack = stack_pop(stack);
				tuple_destroy(curr->value);
				free(curr);

				prev = curr; // needed in order to know if we are coming up from the right
				curr = EMPTY_TREE; // must go upward in the next iteration
				continue;
			} else { // there are nodes to the right which haven't been visited yet
				curr = top->right;
			}
		}
		// save this node for later and go left
		stack = stack_push(stack, curr);
		curr = curr->left;
	}
	
	stack_destroy(stack);

	return NULL;
}

tree_t tree_add(tree_t tree, tuple_t value) {
	index_t index = tuple_fst(value);
	tree_t* curr = &tree;
	
	while (*curr != EMPTY_TREE) {
		tree_t node = *curr; // lessen syntax stress
		index_t curr_index = tuple_fst(node->value);
		if (index_is_less_than(index, curr_index))
			curr = &node->left;
		else if (index_is_less_than(curr_index, index))
			curr = &node->right;
		else // the element already exists, do nothing. TODO: you sure?
			return tree;
	}
	*curr = tree_leaf(value);

	return tree;
}

/**
 * tree_extract_max(tree, &out_max)
 * 
 * Given the tree "tree", find the maximum node value, copy it in the argument pointer
 * "out_max" and remove it from the given tree.
 * Return the argument tree without the maximum.
 *
 * PRE: both tree and out_max must not be NULL.
 */
static inline tree_t tree_extract_max(tree_t tree, tuple_t* out_max) {
	assert(tree != EMPTY_TREE);
	assert(out_max != NULL);
	
	tree_t* curr = &tree;
	
	while ((*curr)->right != EMPTY_TREE) { // keep looking for the max, which will be to the right, of course
		curr = &(*curr)->right;
	}
	// here **curr is the maximum node
	*out_max = (*curr)->value; // we deliver the max
	tree_t left = (*curr)->left; // save the left tree, which might not be empty
	free(*curr); // free this node; don't worry the max won't be lost
	*curr = left; // the rest of the tree remains
	
	return tree;
}

/**
 * tree_remove(tree, index)
 * 
 * PRE: index must not be NULL.
 */
tree_t tree_remove(tree_t tree, index_t index) {
	assert(index != NULL);

	tree_t* curr = &tree;
	
	while (*curr != EMPTY_TREE) {
		tree_t node = *curr; // lessen syntax stress
		index_t curr_index = tuple_fst(node->value);
		if (index_is_less_than(index, curr_index))
			curr = &node->left;
		else if (index_is_less_than(curr_index, index))
			curr = &node->right;
		else { // we found the element
			tuple_destroy(node->value); // destroy the element
			if (node->left == EMPTY_TREE) { // this node has at most one child, we lift that child
				tree_t right = node->right;
				free(node);
				*curr = right;
			} else if (node->right == EMPTY_TREE) { // this node has at most one child, we lift that child
				tree_t left = node->left;
				free(node);
				*curr = left;
			} else { // from the left subtree, put the max here and remove it from that left subtree
				node->left = tree_extract_max(node->left, &node->value);
			}			
		}
	}

	return tree;
}

/**
 * This version doesn't use tree_add: faster.
 */
tree_t tree_copy(tree_t tree) {
	tree_t copy = EMPTY_TREE;
	tree_t* copy_curr = &copy;

	stack_t stack = stack_empty();
	tree_t curr = tree;

	// pre-order iterative traversal	
	while (curr != EMPTY_TREE || !stack_is_empty(stack)) {
		if (curr == EMPTY_TREE) { // now it's time to visit those nodes that were to the right
			curr = stack_top(stack);
			stack = stack_pop(stack);
			copy_curr = stack_top(stack);
			stack = stack_pop(stack);
		}
		// deal with this node right now
		*copy_curr = tree_leaf(tuple_copy(curr->value));
		// later we'll go to the right
		if (curr->right != EMPTY_TREE) {
			stack = stack_push(stack, &(*copy_curr)->right); // ^.^! we use the same stack! low-level ftw!
			stack = stack_push(stack, curr->right);
		}
		// but now go left
		curr = curr->left;
		copy_curr = &(*copy_curr)->left;
	}
	
	stack_destroy(stack);

	return copy;
}

tree_t tree_copy2(tree_t tree) {
	tree_t copy = EMPTY_TREE;

	stack_t stack = stack_empty();
	tree_t curr = tree;

	// pre-order iterative traversal	
	while (curr != EMPTY_TREE || !stack_is_empty(stack)) {
		if (curr == EMPTY_TREE) { // now it's time to visit those nodes that were to the right
			curr = stack_top(stack);
			stack = stack_pop(stack);
		}
		// deal with this node right now
		copy = tree_add(copy, tuple_copy(curr->value));
		// later we'll go to the right
		if (curr->right != EMPTY_TREE)
			stack = stack_push(stack, curr->right);
		// but now go left
		curr = curr->left;
	}
	
	stack_destroy(stack);

	return copy;
}

data_t tree_search(tree_t tree, index_t index) {
	assert(index != NULL);
	
	tree_t curr = tree;
	
	while (curr != EMPTY_TREE) {
		index_t curr_index = tuple_fst(curr->value);
		if (index_is_less_than(index, curr_index))
			curr = curr->left;
		else if (index_is_less_than(curr_index, index))
			curr = curr->right;
		else // we found the element
			return tuple_snd(curr->value);
	}

	return 0; // the element does not exist
}

/**
 * tree_flatten(tree, list)
 * 
 * Flatten tree, in-order style.
 */
/*linked_list_t tree_flatten_inorder(tree_t tree, linked_list_t list) {
	assert(list != NULL);
	
	stack_t stack = stack_empty();
	tree_t curr = tree;

	// in-order iterative traversal	
	while (curr != EMPTY_TREE || !stack_is_empty(stack)) {
		if (curr == EMPTY_TREE) { // no more nodes to the left
			// deal with the parent
			curr = stack_top(stack);
			stack = stack_pop(stack);
			list = list_add(list, index_copy(tuple_fst(curr->value)), data_copy(tuple_snd(curr->value)));
			// now go right
			curr = curr->right;
		} else {
			// save this node for later and go left
			stack = stack_push(stack, curr);
			curr = curr->left;
		}
	}
	
	stack_destroy(stack);

	return list;
}*/

/**
 * tree_flatten(tree, list)
 * 
 * Flatten tree, pre-order style.
 */
/*linked_list_t tree_flatten_preorder(tree_t tree, linked_list_t list) {
	assert(list != NULL);
	
	stack_t stack = stack_empty();
	tree_t curr = tree;

	// pre-order iterative traversal	
	while (curr != EMPTY_TREE || !stack_is_empty(stack)) {
		if (curr == EMPTY_TREE) { // now it's time to visit those nodes that were to the right
			curr = stack_top(stack);
			stack = stack_pop(stack);
		}
		// deal with this node right now
		list = list_add(list, index_copy(tuple_fst(curr->value)), data_copy(tuple_snd(curr->value)));
		// later we'll go to the right
		if (curr->right != EMPTY_TREE)
			stack = stack_push(stack, curr->right);
		// but now go left
		curr = curr->left;
	}
	
	stack_destroy(stack);

	return list;
}*/

/**
 * tree_flatten(tree, list)
 * 
 * Flatten tree, post-order style.
 */
/*linked_list_t tree_flatten_postorder(tree_t tree, linked_list_t list) {
	assert(list != NULL);
	
	stack_t stack = stack_empty();
	tree_t curr = tree;
	tree_t prev = NULL;

	// post-order iterative traversal
	while (curr != EMPTY_TREE || !stack_is_empty(stack)) {
		if (curr == EMPTY_TREE) { // no more nodes to the left
			tree_t top = stack_top(stack);
			if (top->right == EMPTY_TREE || top->right == prev) { // nor to the right
				// so deal with this node
				curr = top;
				stack = stack_pop(stack);
				list = list_add(list, index_copy(tuple_fst(curr->value)), data_copy(tuple_snd(curr->value)));

				prev = curr; // needed in order to know if we are coming up from the right
				curr = EMPTY_TREE; // must go upward in the next iteration
				continue;
			} else { // there are nodes to the right which haven't been visited yet
				curr = top->right;
			}
		}
		// save this node for later and go left
		stack = stack_push(stack, curr);
		curr = curr->left;
	}
	
	stack_destroy(stack);

	return list;
}*/
