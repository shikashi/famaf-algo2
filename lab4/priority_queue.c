#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include "graph.h"
#include "heap.h"
#include "priority_queue.h"

struct _priority_queue_t {
	heap_t heap;
};

priority_queue_t priority_queue_empty(unsigned int max_size) {
	priority_queue_t pq = calloc(1, sizeof(struct _priority_queue_t));
	assert(pq != NULL);
	
	pq->heap = heap_empty(max_size);
	return(pq);
}

priority_queue_t priority_queue_destroy(priority_queue_t pq) {
	assert(pq != NULL);

	heap_destroy(pq->heap);
	free(pq);

	return NULL;
}

priority_queue_t priority_queue_enqueue(priority_queue_t pq, priority_queue_elem_t elem) {
	assert(pq != NULL);
	assert(elem != NULL);
	
	pq->heap = heap_insert(pq->heap, elem);

	return pq;
}

priority_queue_elem_t priority_queue_first(priority_queue_t pq) {
	assert(pq != NULL);

	return heap_minimum(pq->heap);
}

priority_queue_t priority_queue_dequeue(priority_queue_t pq) {
	assert(pq != NULL);

	pq->heap = heap_remove_minimum(pq->heap);

	return pq;
}

bool priority_queue_is_empty(priority_queue_t pq) {
	assert(pq != NULL);
	
	return heap_is_empty(pq->heap);
}

bool priority_queue_is_full(priority_queue_t pq) {
	assert(pq != NULL);

	return heap_is_full(pq->heap);
}

unsigned int priority_queue_size(priority_queue_t pq) {
	assert(pq != NULL);

	return heap_size(pq->heap);
}

void priority_queue_dump(priority_queue_t pq, FILE *fd) {
	assert(pq != NULL);

	heap_dump(pq->heap, fd);
}
