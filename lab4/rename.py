#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import sys

GRAPH_LINE_REGEX = re.compile(r'\s*(\d+)\s*--\s*(\d+)\s*(.*)')
NODE_MAPPING = {
    1: u'Córdoba',
    2: u'La_Rioja',
    3: u'San_Juan',
    4: u'Mendoza',
    5: u'Bariloche',
    6: u'Realicó',
    7: u'Mar_del_Plata',
    8: u'La_Plata',
    9: u'Buenos_Aires',
    10: u'Rosario',
    11: u'Santa_Fe',
    12: u'Santiago_del_Estero',
    13: u'Villa_María',
    14: u'Río_Cuarto',
    15: u'Catamarca',
}


def rename(input_file, output_file):
    for line in input_file.readlines():
        match = GRAPH_LINE_REGEX.match(line)
        if match is not None:
            vertex_from, vertex_to, rest = match.groups()
            line = u'    {0} -- {1} {2}\n'.format(
                        NODE_MAPPING[int(vertex_from)],
                        NODE_MAPPING[int(vertex_to)], rest).encode('utf-8')
        output_file.write(line)


if __name__ == '__main__':
    rename(sys.stdin, sys.stdout)
