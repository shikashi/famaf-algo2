#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

#include "bst.h"
#include "union_find.h"

struct _union_find_t {
	int *classes;
	unsigned int count; // why count and not size?
	unsigned int max_size; // this is only necessary for an assert in push
	bst_t bst;
};

union_find_t union_find_create(unsigned int max_size) {
	union_find_t uf = calloc(1, sizeof(struct _union_find_t));
	assert(uf != NULL);
	
	uf->classes = calloc(max_size + 1, sizeof(int));
	assert(uf->classes != NULL);

	uf->bst = bst_empty();

	uf->count = 0;
	uf->max_size = max_size;
	// uf->classes[i] == 0, for all i in range
	
	return(uf);
}

union_find_t union_find_destroy(union_find_t uf) {
	assert(uf != NULL);
	assert(uf->classes != NULL);

	bst_destroy(uf->bst);
	free(uf->classes);
	free(uf);

	return NULL;
}

/*bool union_find_exists(union_find_t uf, union_find_elem_t elem) {
	assert(uf != NULL);

	

	//return(uf->classes[elem] != 0);
	return ix != 0;
}*/

bool union_find_is_rep(union_find_t uf, union_find_class_t cl) {
	assert(uf != NULL);

	return(uf->classes[cl] < 0);
}

union_find_t union_find_push(union_find_t uf, union_find_elem_t elem) {
	assert(uf != NULL);

	data_t ix = bst_search(uf->bst, elem);

	if (ix == 0) {
		ix = bst_length(uf->bst) + 1;
		uf->bst = bst_add(uf->bst, index_copy(elem), ix);
		uf->classes[ix] = -1;
		uf->count++;
	}

	return(uf);
}

union_find_t union_find_union(union_find_t uf, union_find_class_t class1, union_find_class_t class2) {
	assert(uf != NULL);
	assert(union_find_is_rep(uf, class1));
	assert(union_find_is_rep(uf, class2));
	assert(class1 != class2); // extra

	if (uf->classes[class1] < uf->classes[class2]) {
		uf->classes[class1] += uf->classes[class2];
		uf->classes[class2] = class1;
	} else {
		uf->classes[class2] += uf->classes[class1];
		uf->classes[class1] = class2;
	}

	uf->count--;

	return(uf);
}

union_find_class_t union_find_find(union_find_t uf, union_find_elem_t elem) {
	assert(uf != NULL);

	data_t ix = bst_search(uf->bst, elem);
	assert(ix != 0); // elem must have been previously pushed

	union_find_class_t maybe_class = ix;
	while (!union_find_is_rep(uf, maybe_class))
		maybe_class = uf->classes[maybe_class];

	// path compression
	union_find_class_t the_class = maybe_class;
	maybe_class = ix;
	while (maybe_class != the_class) {
		union_find_class_t next = uf->classes[maybe_class];
		uf->classes[maybe_class] = the_class;
		maybe_class = next;
	}

	return the_class;
}

unsigned int union_find_class_count(union_find_t uf) {
	assert(uf != NULL);

	return(uf->count);
}
