#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include "data.h"

data_t data_from_nat(unsigned data) {
	assert(data != 0);
    return(data);
}

unsigned data_to_nat(data_t data) {
	assert(data != 0);
    return(data); // lol
}

data_t data_copy(data_t data) {
	assert(data != 0);
    return(data); // lol
}

data_t data_destroy(data_t data) {
	assert(data != 0);
	data = 0; // unusued parameter -_-!
	return 0; // whatever
}
