#ifndef _TREE_H
#define _TREE_H

#include "data.h"
#include "index.h"
#include "tuple.h"

typedef struct _tree_t *tree_t;

extern const tree_t EMPTY_TREE;

/**
 * Create a new node with the given value.
 * PRE: value must not be NULL.
 */
tree_t tree_leaf(tuple_t value);

/**
 * Destroy the whole tree starting at "tree".
 * Notice that tree may be NULL.
 */
tree_t tree_destroy(tree_t tree);

/**
 * It won't add if the index already exists.
 * Notice that tree may be NULL.
 */
tree_t tree_add(tree_t tree, tuple_t value);

/**
 * Remove the node with the given index.
 * Notice that tree may be NULL.
 */
tree_t tree_remove(tree_t tree, index_t index);

/**
 * Copy a tree.
 * Notice that tree may be NULL.
 */
tree_t tree_copy(tree_t tree);

/**
 * Return a cloned tree of the selected tree
 * Notice that tree may be NULL.
 */
data_t tree_search(tree_t tree, index_t index);

/**
 * Return an array containing the node values in-order.
 * The length of the array is length.
 * Notice that tree may be NULL.
 */
tuple_t* tree_flatten_inorder_array(tree_t tree, size_t length);

/**
 * PRE: list must not be NULL
 * Notice that tree may be NULL.
 */
//linked_list_t tree_flatten_inorder(tree_t tree, linked_list_t list);

/**
 * PRE: list must not be NULL
 * Notice that tree may be NULL.
 */
//linked_list_t tree_flatten_preorder(tree_t tree, linked_list_t list);

/**
 * PRE: list must not be NULL
 * Notice that tree may be NULL.
 */
//linked_list_t tree_flatten_postorder(tree_t tree, linked_list_t list);

#endif
