#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "graph.h"
#include "stack_edge.h"

typedef struct _node_t *node_t;

struct _node_t {
	stack_elem_t data;
	node_t next;
};

stack_t stack_empty(void) {
	return NULL;
}

stack_t stack_push(stack_t stack, stack_elem_t data) {
	node_t n = calloc(1, sizeof(struct _node_t));
	assert(n != NULL);

	n->data = data;
	n->next = stack;
	return n;
}

stack_t stack_pop(stack_t stack) {
	assert(stack != NULL);

	node_t next = stack->next;
	stack_elem_destroy(stack->data);
	free(stack);

	return next;
}

stack_elem_t stack_top(stack_t stack) {
	assert(stack != NULL);

	return stack_elem_copy(stack->data);
}

bool stack_is_empty(stack_t stack) {
	return stack == NULL;
}

stack_t stack_destroy(stack_t stack) {
	while (!stack_is_empty(stack))
		stack = stack_pop(stack);

	return NULL;
}
