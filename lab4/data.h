#ifndef _DATA_H
#define _DATA_H

#include <stdbool.h>


typedef unsigned int data_t;

unsigned data_to_nat(data_t data);

data_t data_from_nat(unsigned source);

data_t data_copy(data_t data);

data_t data_destroy(data_t data);

#endif
