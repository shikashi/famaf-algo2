#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include "graph.h"
#include "heap.h"

struct _heap_t {
	unsigned int max_size;
	heap_elem_t *elems; // The first element in the array will be a dummy element (a centinel).
	unsigned int size;
};

////////////////////////////////////////////////////////////////////////////////
// Auxiliary functions
////////////////////////////////////////////////////////////////////////////////

unsigned int heap_left_son(unsigned int father) {
	return(father * 2);
}

unsigned int heap_right_son(unsigned int father) {
	return(father * 2 + 1);
}

unsigned int heap_father(unsigned int child) {
	return(child / 2);
}

bool heap_has_children(heap_t heap, unsigned int father) {
	assert(heap != NULL);

	return(heap_left_son(father) <= heap->size);
}

bool heap_has_father(unsigned int elem) {
	return elem > 1;
}

unsigned int heap_min_child(heap_t heap, unsigned int father) {
	assert(heap != NULL);
	assert(1 <= father && father <= heap->size);
	assert(heap_has_children(heap, father));

	unsigned left = heap_left_son(father);
	unsigned right = heap_right_son(father);
	if (right <= heap->size && heap_elem_lt(heap->elems[right], heap->elems[left])) // lt and lte are both equally usable for our purposes
		return(right);
	else
		return(left);
}

void heap_swap(heap_t heap, unsigned int i, unsigned int j) {
	assert(heap != NULL);

	heap_elem_t tmp = heap->elems[i];
	heap->elems[i] = heap->elems[j];
	heap->elems[j] = tmp;
}

void heap_lift(heap_t heap, unsigned int child) {
	assert(heap != NULL);
	assert(1 <= child && child <= heap->size);
	assert(heap_has_father(child));

	heap_swap(heap, child, heap_father(child));
}

bool heap_must_lift(heap_t heap, unsigned int child) {
	assert(heap != NULL);
	assert(1 <= child && child <= heap->size);
	assert(heap_has_father(child));

	return(heap_elem_lt(heap->elems[child], heap->elems[heap_father(child)]));
}

heap_t heap_sink(heap_t heap) {
	assert(heap != NULL);

	unsigned int f = 1;
	while (heap_has_children(heap, f) && heap_must_lift(heap, heap_min_child(heap, f))) {
		f = heap_min_child(heap, f);
		heap_lift(heap, f);
	}

	return(heap);
}

heap_t heap_float(heap_t heap) {
	assert(heap != NULL);

	unsigned int c = heap->size;
	while (heap_has_father(c) && heap_must_lift(heap, c)) {
		heap_lift(heap, c);
		c = heap_father(c);
	}

	return heap;
}

////////////////////////////////////////////////////////////////////////////////
// Heap iterface functions
////////////////////////////////////////////////////////////////////////////////

heap_t heap_empty(unsigned int max_size) {
	heap_t h = calloc(1, sizeof(struct _heap_t));
	assert(h != NULL);

	h->elems = calloc(max_size + 1, sizeof(heap_elem_t)); // +1 because the first element is a dummy one
	assert(h->elems != NULL);

	h->max_size = max_size;
	h->size = 0; // bleh // duh

	return(h); // do you renember that for stupid "coding styles" we MUST do that?
	// I absolutely refuse to use that clunky funky retarded style. period.
	// I also refuse.
	// we simply use sed afterwards
}

heap_t heap_destroy(heap_t heap) {
	// check preconditions
	assert(heap != NULL);

	assert(heap->elems);

	for(unsigned i = 1; i <= heap->size; i++)
		heap_elem_destroy(heap->elems[i]);
	free(heap->elems);
	free(heap);

	return(NULL);
}

heap_t heap_insert(heap_t heap, heap_elem_t elem) {

	// check preconditions
	assert(heap != NULL);
	assert(!heap_is_full(heap));

	unsigned int old_size = heap->size;
	heap->size++;
	heap->elems[heap->size] = elem;
	heap = heap_float(heap);

	// check postconditions
	assert(old_size + 1 == heap->size);

	return(heap);
}

heap_t heap_remove_minimum(heap_t heap) {
	assert(heap != NULL);
	assert(!heap_is_empty(heap));

	heap_elem_destroy(heap->elems[1]);
	heap->elems[1] = heap->elems[heap->size];
	heap->size--;
	heap = heap_sink(heap);
	
	return heap;
}

heap_elem_t heap_minimum(heap_t heap) {
	assert(heap != NULL);
	assert(!heap_is_empty(heap));

	return heap_elem_copy(heap->elems[1]);
}

bool heap_is_empty(heap_t heap) {
	assert(heap != NULL);
	return(heap->size == 0);
}

bool heap_is_full(heap_t heap) {
	assert(heap != NULL);
	return(heap->size == heap->max_size);
}

unsigned int heap_size(heap_t heap) {
	assert(heap != NULL);
	return(heap->size);
}

void heap_dump(heap_t heap, FILE *fd) { // debug
	assert(heap != NULL);

	for (unsigned i = 1; i <= heap->size; i++) {
		fprintf(fd, "%d ", edge_weight(heap->elems[i]));
	}
	fprintf(fd, "\n");
}
