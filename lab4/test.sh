#!/bin/sh

input="$1"

valgrind --leak-check=full --show-reachable=yes ./kruskal < "$input" > input/out.dot
neato -Tpng -o input/out.png input/out.dot
xdg-open input/out.png
