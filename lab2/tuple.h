#ifndef _TUPLE_H
#define _TUPLE_H

#include "index.h"
#include "data.h"

typedef struct _tuple_t *tuple_t;


tuple_t tuple_from_index_data(index_t index, data_t data);
/*
 * Build a new tuple from the given index and data using references to them.
 *
 * Do NOT free index and data after creating the tuple, but only through
 * tuple_destroy.
 *
 * PRE: 'index' and 'data' must be not NULL.
 */

tuple_t tuple_copy(tuple_t t);
/*
 * Return a cloned copy of the given tuple.
 *
 * PRE: 't' must be not NULL.
 */

tuple_t tuple_destroy(tuple_t t);
/*
 * Free the memory allocated by given tuple, as well as the respective
 * index and data. Set 't' to NULL.
 *
 * PRE: 't' must be not NULL.
 */

index_t tuple_fst(tuple_t t);
/*
 * Return a reference to the first tuple element.
 *
 * PRE: 't' must be not NULL.
 */

data_t tuple_snd(tuple_t t);
/*
 * Return a reference to the second tuple element.
 *
 * PRE: 't' must be not NULL.
 */

#endif
