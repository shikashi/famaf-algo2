#ifndef _LINKED_LIST_H
#define _LINKED_LIST_H

#include <stdio.h>

#include "data.h"
#include "index.h"


typedef struct _linked_list_t *linked_list_t;

linked_list_t list_empty(void);
/*
 * Return a newly created, empty list.
 *
 * The caller must call list_destroy when done using the resulting list,
 * so the resources allocated by this call are freed.
 *
 * POST: the result will not be NULL, and list_length(result) will be 0.
 */

linked_list_t list_copy(linked_list_t list);
/*
 * Return a newly created copy of the given 'list'.
 *
 * The caller must call list_destroy when done using the resulting list,
 * so the resources allocated by this call are freed.
 *
 * POST: the result will not be NULL and it will be an exact copy of 'list'.
 * In particular, list_length(result) will be equal to the length of 'list'.
 */

unsigned int list_length(linked_list_t list);
/*
 * Return the number of elements in the given 'list'.
 * Constant order complexity.
 *
 * PRE: 'list' must not be NULL.
 */

linked_list_t list_add(linked_list_t list, index_t index, data_t data);
/*
 * Return a list equals to 'list' with the ('index', 'data') pair added.
 *
 * The given 'index' and 'data' are inserted in the list, so do not destroy them.
 *
 * PRE: all 'list', 'index' and 'data' must be not NULL.
 *
 * POST: the length of the result will be the same as the length of 'list'
 * plus one. The elements of the result will be the same as the one in 'list'
 * with the new pair ('index', 'data') added.
 */

linked_list_t list_remove(linked_list_t list, index_t index);
/*
 * Return a list equals to 'list' with the first (index, <data>) tuple occurrence
 * removed.
 *
 * Please note that 'index' may not be in the list (thus an unchanged
 * list is returned).
 *
 * PRE: both 'list' and 'index' must not be NULL.
 *
 * POST: the length of the result will be the same as the length of 'list'
 * minus one if 'index' existed in 'list'. The elements of the result will be
 * the same as the one in 'list' with the entry for'word' removed.
 */

data_t list_search(linked_list_t list, index_t index);
/*
 * Return the data associated to the first (index, *) tuple occurrence in list,
 * or NULL if the given 'index' is not in 'list'.
 *
 * The caller must NOT free the resources allocated for the result when done
 * using it.
 *
 * PRE: both 'list' and 'index' must not be NULL.
 */

linked_list_t list_destroy(linked_list_t list);
/*
 * Free the resources allocated for the given 'list', and set it to NULL.
 *
 * PRE: 'list' must not be NULL.
 */

void list_dump(linked_list_t list, FILE *fd);
/*
 * Dump the list elements using their string representation to the given
 * file descriptor.
 *
 * PRE: 'list' must not be NULL, and 'fd' must be a valid file descriptor..
 */

#endif
