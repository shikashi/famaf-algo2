#include <assert.h>
#include <stdlib.h>

#include "dict.h"
#include "tuple.h"
#include "linked_list.h"

struct _dict_t {
	linked_list_t entries;
	tuple_t cache; // it will save only 1 word with its def
};


/**
 * PRE: d != NULL && i != NULL
 */
static inline bool is_cached(dict_t d, index_t i) {
	return(d->cache != NULL && index_is_equal(i, tuple_fst(d->cache)));
}

/**
 * PRE: d != NULL
 */
static inline void delete_cache(dict_t d) {
	if (d->cache != NULL)
		d->cache = tuple_destroy(d->cache);
}

/**
 * PRE: d != NULL && i != NULL && a != NULL
 */
static inline void set_cache(dict_t d, index_t i, data_t a) {
	delete_cache(d);
	d->cache = tuple_from_index_data(index_copy(i), data_copy(a));
	// the tuple is now the owner of the index and the data elements so we don't need to free them
}


dict_t dict_empty(void) {
	dict_t d = calloc(1, sizeof(struct _dict_t));

	assert(d != NULL); // POST

	d->entries = list_empty();

	d->cache = NULL; // unnecessary, calloc will zero-out all the memory

	return(d);
}


unsigned int dict_length(dict_t dict) {

	// Check preconditions
	assert(dict != NULL);

	return(list_length(dict->entries));
}


dict_t dict_add(dict_t dict, word_t word, def_t def) {

	// check preconditions
	assert(dict != NULL);
	assert(word != NULL);
	assert(def != NULL);

	dict->entries = list_add(dict->entries, index_from_string(word), data_from_string(def));
	// the list is now the owner of the index and the data elements so we don't free them

	// TODO: maybe we should cache the element here too...

	// check postconditions
	// list_add's POST implies dict_add's POST

	return(dict);
}


bool dict_exists(dict_t dict, word_t word) {

	// check preconditions
	assert(word != NULL);
	assert(dict != NULL);

	index_t word_as_index = index_from_string(word);
	data_t def_as_data = list_search(dict->entries, word_as_index);

	if (def_as_data != NULL)
		set_cache(dict, word_as_index, def_as_data);

	index_destroy(word_as_index);

	return(def_as_data != NULL);
}

dict_t dict_remove(dict_t dict, word_t word) {

	// check preconditions
	assert(dict != NULL);
	assert(word != NULL);

	index_t word_as_index = index_from_string(word);

	if (is_cached(dict, word_as_index))
		delete_cache(dict); // clear the cache since we are going to remove the element

	dict->entries = list_remove(dict->entries, word_as_index);

	index_destroy(word_as_index);

	// check postconditions
	// list_remove's POST implies dict_remove's POST

	return(dict);
}


def_t dict_search(dict_t dict, word_t word) {
	index_t word_as_index = index_from_string(word);
	
	if (!is_cached(dict, word_as_index)) {
		/* We do it this way because otherwise compiling with assertions disabled will
		   wreak mayhem. */
		bool e = dict_exists(dict, word);
		assert(e);
	}
	// At this point, if the word exists then it's cached.

	index_destroy(word_as_index);

	// check postconditions
	// PRE => r != NULL

	return(data_to_string(tuple_snd(dict->cache)));
}


dict_t dict_destroy(dict_t dict) {

	// check preconditions
	assert(dict != NULL);

	dict->entries = list_destroy(dict->entries); // the assignment is kind of unnecessary since the whole structure is going to be deleted

	/*if (dict->cache != NULL)
		dict->cache = tuple_destroy(dict->cache); // see previous comment*/
	delete_cache(dict);

	free(dict);
	dict = NULL;

	return(dict);
}


linked_list_t dict_to_linked_list(dict_t dict) {

	// check preconditions
	assert(dict != NULL);

	// check postconditions
	// list_copy's POST implies dict_to_linked_list's POST

	return(list_copy(dict->entries));
}


void dict_dump(dict_t dict) {

	// check preconditions
	assert(dict != NULL);

	list_dump(dict->entries, stdout);
}
