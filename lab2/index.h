#ifndef _INDEX_H
#define _INDEX_H

#include <stdbool.h>


typedef struct _index_t *index_t;

unsigned int index_max_length(void);
/*
 * Return the maximum length for the string representation of any index.
 */

unsigned int index_length(index_t index);
/*
 * Return the length of the given 'index'.
 *
 * PRE: 'index' must be not NULL.
 */

bool index_is_equal(index_t index, index_t other);
/*
 * Return whether the given 'index' is equal to 'other'.
 *
 * PRE: both 'index' and 'other' must be not NULL.
 */

bool index_is_less_than(index_t index, index_t other);
/*
 * Return whether the given 'index' is less than 'other' (alphabetically sorted).
 *
 * PRE: both 'index' and 'other' must be not NULL.
 */

char *index_to_string(index_t index);
/*
 * Return the string representation of the given 'index'.
 *
 * The caller is responsible for the allocated reources for the result, thus
 * those should be freed when done using it.
 *
 * PRE: 'index' must be not NULL.
 *
 * POST: the result will a not NULL, null-terminated string.
 */

index_t index_from_string(char *source);
/*
 * Return a newly created index from the given string 'source'.
 *
 * The string 'source' must be null-terminated, and this function will
 * make a copy of it, so is the caller's responsability to free it.
 *
 * PRE: 'source' must be not NULL.
 *
 * POST: the result will be not NULL, and the string representation of it
 * will be equal to 'source'.
 */

index_t index_copy(index_t index);
/*
 * Return a cloned copy of the given 'index'.
 *
 * PRE: 'index' must be not NULL.
 *
 * POST: the result will be not NULL, and the string representation of it
 * will be equal to the string representation of 'data'.
 */

index_t index_destroy(index_t index);
/*
 * Free the resources used by the given 'index', and set it to NULL.
 * 
 * PRE: 'index' must be not NULL.
 */

#endif
