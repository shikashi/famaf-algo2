#ifndef _HELPERS_H
#define _HELPERS_H

#include "dict.h"


char *readline_from_stdin(void);
/*
 * Read the user input from standard input until a newline is detected,
 * and return the corresponding (dinamically allocated) string.
 *
 * The caller to this function is responsible for the allocated memory.
 *
 * POST: A new null-terminated string is returned with the content read from
 *       standard input, or NULL if there was an error.
 */

dict_t dict_from_file(char *filename);
/*
 * Return a dict instance populated by the data in the given filename.
 *
 * PRE: filename is the path to an existent and accessible file.
 *
 * file data is formatted as follows:
 *      word_1: definition_1
 *      ...
 *      word_N: definition_N
 *
 * POST: Returned dict is not NULL and contains the words and respective
 *       definitions listed in the given file.
 */

void dict_to_file(dict_t dict, char *filename);
/*
 * Write dict data to file with the format expected by dict_from_file.
 *
 * PRE: 'dict' must not be NULL.
 *
 * POST: 'dict' words and respective definitions are written to 'filename'
 *       using the format:
 *       word_1: definition_1
 *       ...
 *       word_N: definition_N
 */

#endif
